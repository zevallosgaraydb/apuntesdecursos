import numpy as np
import matplotlib.pyplot as plt

# Definimos los parámetros del autómata celular probabilístico
num_cells = 50  # Número de celdas en la retícula unidimensional
num_steps = 100  # Número de pasos de tiempo
p = 0.5  # Probabilidad de transición

# Inicializamos la configuración del autómata celular (todas las celdas en estado 0 inicialmente)
cells = np.zeros(num_cells, dtype=int)

# Asignamos un estado inicial aleatorio (0 o 1) a cada celda
cells = np.random.choice([0, 1], size=num_cells)

# Almacenamos la evolución del autómata celular a lo largo del tiempo
history = [cells.copy()]

# Función para calcular el nuevo estado de una celda
def new_state(i, cells, p):
    # Definimos el vecindario como las celdas adyacentes
    left = cells[i - 1] if i > 0 else cells[-1]  # Vecino izquierdo (con borde periódico)
    right = cells[i + 1] if i < num_cells - 1 else cells[0]  # Vecino derecho (con borde periódico)
    center = cells[i]  # Estado actual de la celda
    
    # Calculamos la probabilidad de transición
    if left + center + right >= 2:  # Al menos 2 vecinos en estado 1
        return 1 if np.random.random() < p else 0
    else:
        return 0 if np.random.random() < p else 1

# Simulamos la evolución del autómata celular
for _ in range(num_steps):
    new_cells = cells.copy()
    for i in range(num_cells):
        new_cells[i] = new_state(i, cells, p)
    cells = new_cells
    history.append(cells.copy())

# Visualizamos la evolución del autómata celular
plt.figure(figsize=(10, 6))
plt.imshow(history, cmap='binary', interpolation='nearest')
plt.xlabel('Celdas')
plt.ylabel('Tiempo')
plt.title('Evolución de un Autómata Celular Probabilístico')
plt.show()
